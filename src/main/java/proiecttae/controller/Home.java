/**
 *
 * @author aly
 */
package proiecttae.controller;

import ch.qos.logback.core.CoreConstants;
import java.beans.PropertyEditorSupport;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import static org.springframework.data.repository.init.ResourceReader.Type.JSON;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import proiecttae.entities.EventEntity;
import proiecttae.entities.EventRepo;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Controller
public class Home {

    //extends WebMvcConfigurerAdapter
    private final EventRepo eventRepo;

    public Home(EventRepo eventRepo) {
        this.eventRepo = eventRepo;

    }

    @RequestMapping("/")
    public String getAllEvents(Model model) {
        model.addAttribute("event", new EventEntity());
        List<EventEntity> events = eventRepo.findAll();
        model.addAttribute("events", events);
        return "index";
    }

    @RequestMapping("/{id}")
    public String getEvent(Model model, @PathVariable Long id) {
        EventEntity event = eventRepo.findOne(id);
        model.addAttribute("eventEdit", event);

        return "editForm";
    }

    @RequestMapping("/getdeleted/{id}")
    public String getDeletedEvent(Model model, @PathVariable Long id) {
        EventEntity event = eventRepo.findOne(id);
        model.addAttribute("eventDelete", event);

        return "deleteForm";
    }

    @RequestMapping(value = "/addEvent",
            method = RequestMethod.POST)
    public String addEv(@ModelAttribute EventEntity event, BindingResult result) {

        String evId = "";
        try {

            eventRepo.save(event);

        } catch (Exception ex) {
            return "Error: " + ex.toString();
        }

        return "redirect:/";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/edit/{id}")
    public String update(@PathVariable Long id, @ModelAttribute EventEntity updatedEv, BindingResult result, Model model) {
        EventEntity event = eventRepo.findOne(id);
        System.out.println("!!!!!!!!!" + updatedEv.getEventName() + updatedEv.getEventName() + updatedEv.getEventDate().toString());
        model.addAttribute("event", event);
        event.setEventName(updatedEv.getEventName());
        event.setEventDate(updatedEv.getEventDate());
        event.setEventAddress(updatedEv.getEventAddress());
        eventRepo.save(updatedEv);
        return "redirect:/";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public String deleteCategory(@PathVariable(value = "id") long id) {
        eventRepo.delete(id);
        return "redirect:/";
    }

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

}
