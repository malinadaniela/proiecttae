/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proiecttae.entities;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 *
 * @author aly
 */

@Entity
@AllArgsConstructor @NoArgsConstructor

public class EventEntity {
    
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter @Setter private Long id;  
    
    @Getter @Setter private String eventName; 
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Getter @Setter private Date eventDate; 
    @Getter @Setter private String eventAddress;
    /*@ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name="LocationEntity_id")
    @Getter @Setter private LocationEntity eventLocation; */

   
    public EventEntity(String eventName, Date eventDate, String adresa) {
       
        this.eventName = eventName;
        this.eventDate = eventDate;
        this.eventAddress=adresa;
    }
    
    @ModelAttribute("event")
public EventEntity getEvent(){
    return new EventEntity();
}
    
    
    
    
}
